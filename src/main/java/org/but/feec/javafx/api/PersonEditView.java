package org.but.feec.javafx.api;

public class PersonEditView {

    private int id;
    private String givenName;
    private String familyName;
    private int age;
    private String email;
    private String city;
    private int resultID;


    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }
    public String getGivenName() {
        return givenName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }
    public String getFamilyName() {
        return familyName;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return age;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getCity() {
        return city;
    }

    public int getResultID(){return resultID;}
    public void setResultID(int resultID){this.resultID = resultID;}

}
