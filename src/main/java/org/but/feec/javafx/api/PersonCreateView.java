package org.but.feec.javafx.api;

import java.util.Arrays;

public class PersonCreateView {

    private int id;
    private String givenName;
    private String familyName;
    private int age;
    private int result;
    private String email;
    private String city;
    private int resultID;
    private char[] pwd;

    public String getGivenName() {
        return givenName;
    }
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }


    public void setID(int id){this.id = id;}
    public int getID(){return id;}

    public void setAge(int age){this.age = age;}
    public int getAge(){return age;}

    public void setResult(int result) {
        this.result= result;
    }
    public int getResult() {
        return result;
    }

    public int getResultID(){return resultID;}
    public void setResultID(int resultID){this.resultID = resultID;}

    public void setPwd(char[] hashedPassword){
        this.pwd = hashedPassword;
    }
    public char[] getPwd(){
        return  pwd;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public String getCity() {
        return city;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }


    @Override
    public String toString() {
        return "PersonCreateView{" +
                "id='"+id+ '\''+
                ",givenName='" + givenName + '\'' +
                ",surname='" + familyName + '\'' +
                ",age='" + age + '\'' +
                ",result='" + result + '\'' +
                ",email='" + email + '\'' +
                ",city='" + city + '\'' +
                '}';
    }
}
