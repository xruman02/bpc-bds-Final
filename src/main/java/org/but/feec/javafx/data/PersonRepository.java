package org.but.feec.javafx.data;

import org.but.feec.javafx.api.*;
import org.but.feec.javafx.config.DataSourceConfig;
import org.but.feec.javafx.exceptions.DataAccessException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {

    public PersonAuthView findPersonByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT email, pwd, id_person" +
                             " FROM mydb.contact c" +
                             " WHERE c.email = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    public PersonDetailView findPersonDetailedView(Long personId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT p.id_person, given_name, surname, age, city, email, status_type, s.id_status_type FROM mydb.person p \n" +
                             "JOIN mydb.person_has_address pa ON p.id_person = pa.id_person \n" +
                             "JOIN mydb.address a ON pa.id_address = a.id_address JOIN mydb.contact c ON c.id_person = p.id_person\n" +
                             "JOIN mydb.status s ON s.id_person = p.id_person JOIN mydb.status_type st ON st.id_status_type = s.id_status_type" +
                             " WHERE p.id_person = ?")
        ) {
            preparedStatement.setLong(1, personId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    public List<PersonBasicView> getPersonsBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT id_person, surname, given_name" +
                             " FROM mydb.person p" );
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<PersonBasicView> personBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                personBasicViews.add(mapToPersonBasicView(resultSet));
            }
            return personBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Persons basic view could not be loaded.", e);
        }
    }

    public void createPerson(PersonCreateView personCreateView) {
        String insertPersonSQL = "INSERT INTO mydb.person (id_person, surname, given_name, age) VALUES (?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {

            preparedStatement.setString(3, personCreateView.getGivenName());
            preparedStatement.setString(2, personCreateView.getFamilyName());
            preparedStatement.setInt(4, personCreateView.getAge());
            preparedStatement.setInt(1, personCreateView.getID());

            String insertContactSQL = "INSERT INTO mydb.contact (email, password,id_person) VALUES (?,?,?)";
            try (PreparedStatement psContactInsert = connection.prepareStatement(insertContactSQL, Statement.RETURN_GENERATED_KEYS)) {
                psContactInsert.setString(1, personCreateView.getEmail());
                psContactInsert.setString(2, personCreateView.getPwd().toString());
                psContactInsert.setInt(3, personCreateView.getID());
            } catch (SQLException e) {
                throw new DataAccessException("Failed to insert contact.");
            }

            String insertStatusSQL = "INSERT INTO mydb.status (id_person, id_status_type) VALUES (?,?)";
            try (PreparedStatement psStatusUpdate = connection.prepareStatement(insertStatusSQL, Statement.RETURN_GENERATED_KEYS)) {
                psStatusUpdate.setInt(1, personCreateView.getID());
                psStatusUpdate.setInt(2, personCreateView.getResultID());
            } catch (SQLException e) {
                throw new DataAccessException("Failed to insert status.");
            }

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating person failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }

    }

    public void editPerson(PersonEditView personEditView) {
        String updatePersonSQL = "UPDATE mydb.person p SET given_name = ?, surname = ?, age = ? WHERE p.id_person = ?";
        String updateContactSQL = "UPDATE mydb.contact c SET email = ? WHERE c.id_person = ?";
        String updateStatusSQL = "UPDATE mydb.status s SET id_status_type = ? WHERE s.id_person = ?";
        String checkIfExists = "SELECT given_name FROM mydb.person p WHERE p.id_person = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(updatePersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, personEditView.getGivenName());
            preparedStatement.setString(2, personEditView.getFamilyName());
            preparedStatement.setLong(3, personEditView.getAge());
            preparedStatement.setLong(4, personEditView.getId());

            try {
               connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, personEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This persons data is not eligible for editing.");
                }

                try (PreparedStatement psContactUpdate = connection.prepareStatement(updateContactSQL, Statement.RETURN_GENERATED_KEYS)) {
                    psContactUpdate.setString(1, personEditView.getEmail());
                    psContactUpdate.setLong(2, personEditView.getId());
                    psContactUpdate.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This persons data is not eligible for editing.");
                }

                try (PreparedStatement psStatusUpdate = connection.prepareStatement(updateStatusSQL, Statement.RETURN_GENERATED_KEYS)) {
                    psStatusUpdate.setInt(1, personEditView.getResultID());
                    psStatusUpdate.setLong(2, personEditView.getId());
                    psStatusUpdate.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This persons data is not eligible for editing.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating person failed, no rows affected.");
                }

                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }
    }

    private PersonAuthView mapToPersonAuth(ResultSet rs) throws SQLException {
        PersonAuthView person = new PersonAuthView();
        person.setEmail(rs.getString("email"));
        person.setPassword(rs.getString("pwd"));
        return person;
    }

    private PersonBasicView mapToPersonBasicView(ResultSet rs) throws SQLException {
        PersonBasicView personBasicView = new PersonBasicView();
        personBasicView.setId(rs.getLong("id_person"));
        personBasicView.setGivenName(rs.getString("given_name"));
        personBasicView.setFamilyName(rs.getString("surname"));
        return personBasicView;
    }

    private PersonDetailView mapToPersonDetailView(ResultSet rs) throws SQLException {
        PersonDetailView personDetailView = new PersonDetailView();
        personDetailView.setId(rs.getLong("id_person"));
        personDetailView.setGivenName(rs.getString("given_name"));
        personDetailView.setFamilyName(rs.getString("surname"));
        personDetailView.setAge(rs.getString("age"));
        personDetailView.setResult(rs.getString("status_type"));
        personDetailView.setResultID(rs.getString("id_status_type"));
        personDetailView.setEmail(rs.getString("email"));
        personDetailView.setCity(rs.getString("city"));
        return personDetailView;
    }



}
