package org.but.feec.javafx.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.javafx.api.PersonDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonsDetailViewController {

    private static final Logger logger = LoggerFactory.getLogger(PersonsDetailViewController.class);


    @FXML
    private TextField idTxtField;
    @FXML
    private TextField nameTxtField;
    @FXML
    private TextField surnameTxtField;
    @FXML
    private TextField ageTxtField;
    @FXML
    private TextField resultTxtField;
    @FXML
    private TextField emailTxtField;
    @FXML
    private TextField cityTxtField;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTxtField.setEditable(false);
        nameTxtField.setEditable(false);
        surnameTxtField.setEditable(false);
        ageTxtField.setEditable(false);
        resultTxtField.setEditable(false);
        emailTxtField.setEditable(false);
        cityTxtField.setEditable(false);

        loadPersonsData();

        logger.info("PersonsDetailViewController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView) {
            PersonDetailView personBasicView = (PersonDetailView) stage.getUserData();
            idTxtField.setText(String.valueOf(personBasicView.getId()));
            nameTxtField.setText(personBasicView.getGivenName());
            surnameTxtField.setText(personBasicView.getFamilyName());
            ageTxtField.setText(personBasicView.getAge());
            resultTxtField.setText(personBasicView.getResult());
            emailTxtField.setText(personBasicView.getEmail());
            cityTxtField.setText(personBasicView.getCity());
        }
    }


}