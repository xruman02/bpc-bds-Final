package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.javafx.api.PersonBasicView;
import org.but.feec.javafx.api.PersonDetailView;
import org.but.feec.javafx.api.PersonEditView;
import org.but.feec.javafx.data.PersonRepository;
import org.but.feec.javafx.services.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;


public class PersonsEditController {

    private static final Logger logger = LoggerFactory.getLogger(PersonsEditController.class);

    @FXML
    public Button editPersonButton;
    @FXML
    public TextField idTextField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField givenNameTextField;
    @FXML
    private TextField familyNameTextField;

    @FXML
    private TextField ageTextField;
    @FXML
    private TextField resultTextField;
    @FXML
    private TextField cityTextField;
    @FXML
    private ChoiceBox<String> resultChoiceBox;

    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    private String[] resultChoices = {"Vaccinated", "Recovered", "Tested positive", "Tested negative", "Hospitalized", "Passed away"};

    // used to reference the stage and to get passed data through it
    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {




        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);
        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(emailTextField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(givenNameTextField, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(familyNameTextField, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(resultTextField, Validator.createEmptyValidator("Result must not be empty."));
        validation.registerValidator(cityTextField, Validator.createEmptyValidator("City must not be empty."));
        validation.registerValidator(ageTextField, Validator.createEmptyValidator("Age must not be empty."));

        resultChoiceBox.getItems().addAll(resultChoices);
        resultChoiceBox.setOnAction(this::newChoice);

        editPersonButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("PersonsEditController initialized");

        resultTextField.setEditable(false);
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof PersonDetailView) {

            PersonDetailView personDetailView = (PersonDetailView) stage.getUserData();
            resultChoiceBox.setValue(personDetailView.getResult());
            idTextField.setText(String.valueOf(personDetailView.getId()));
            emailTextField.setText(personDetailView.getEmail());
            givenNameTextField.setText(personDetailView.getGivenName());
            familyNameTextField.setText(personDetailView.getFamilyName());
            ageTextField.setText(personDetailView.getAge());
            resultTextField.setText(personDetailView.getResultID());
            cityTextField.setText(personDetailView.getCity());
        }
    }

    @FXML
    public void handleEditPersonButton(ActionEvent event) {

        PersonEditView personEditView = new PersonEditView();
        personEditView.setId(Integer.parseInt(idTextField.getText()));
        personEditView.setGivenName(givenNameTextField.getText());
        personEditView.setFamilyName(familyNameTextField.getText());
        personEditView.setAge(Integer.parseInt(ageTextField.getText()));
        personEditView.setResultID(Integer.parseInt(resultTextField.getText()));
        personEditView.setEmail(emailTextField.getText());
        personEditView.setCity(cityTextField.getText());

        personService.editPerson(personEditView);

        personEditedConfirmationDialog();
    }

    @FXML
    public void newChoice(ActionEvent actionEvent) {
        String newResult = resultChoiceBox.getValue();
        switch (newResult) {
            case "Vaccinated":
                resultTextField.setText("1");
                break;

            case "Recovered":
                resultTextField.setText("2");
                break;

            case "Tested positive":
                resultTextField.setText("3");
                break;

            case "Tested negative":
                resultTextField.setText("4");
                break;

            case "Hospitalized":
                resultTextField.setText("5");
                break;

            case "Passed away":
                resultTextField.setText("6");
                break;
        }
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Edited Confirmation");
        alert.setHeaderText("Your person was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
