package org.but.feec.javafx.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Duration;
import org.but.feec.javafx.api.PersonCreateView;
import org.but.feec.javafx.api.PersonEditView;
import org.but.feec.javafx.data.PersonRepository;
import org.but.feec.javafx.services.PersonService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Text;

import java.util.Optional;

public class PersonCreateController {

    private static final Logger logger = LoggerFactory.getLogger(PersonCreateController.class);

    @FXML
    public Button createPersonButton;
    @FXML
    public TextField newID;
    @FXML
    private TextField newemailTextField;
    @FXML
    private TextField newgivenNameTextField;
    @FXML
    private TextField newfamilyNameTextField;
    @FXML
    private TextField newcityTextField;
    @FXML
    private TextField newageTextField;
    @FXML
    private TextField Result;
    @FXML
    private ChoiceBox<String> newresultChoiceBox;
    @FXML
    private TextField newPwd;


    private PersonService personService;
    private PersonRepository personRepository;
    private ValidationSupport validation;
    private String[] resultChoices = {"Vaccinated", "Recovered", "Tested positive", "Tested negative", "Hospitalized", "Passed away"};

    @FXML
    public void initialize() {
        newresultChoiceBox.getItems().addAll(resultChoices);
        newresultChoiceBox.setOnAction(this::createChoice);
        personRepository = new PersonRepository();
        personService = new PersonService(personRepository);
        Result.setEditable(false);


        validation = new ValidationSupport();
        validation.registerValidator(newemailTextField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(newgivenNameTextField, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(newfamilyNameTextField, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(newageTextField, Validator.createEmptyValidator("Age must not be empty."));
        validation.registerValidator(newcityTextField, Validator.createEmptyValidator("City name must not be empty."));
        validation.registerValidator(Result, Validator.createEmptyValidator("Result must not be empty."));
        validation.registerValidator(newPwd, Validator.createEmptyValidator("Password must not be empty."));


        createPersonButton.disableProperty().bind(validation.invalidProperty());

        logger.info("PersonCreateController initialized");
    }

    @FXML
    void handleCreateNewPerson(ActionEvent event) {
        PersonCreateView personCreateView = new PersonCreateView();
        personCreateView.setGivenName(newgivenNameTextField.getText());
        personCreateView.setFamilyName(newfamilyNameTextField.getText());
        personCreateView.setAge(Integer.valueOf(newageTextField.getText()));
        personCreateView.setResult(Integer.parseInt(Result.getText()));
        personCreateView.setEmail(newemailTextField.getText());
        personCreateView.setCity(newcityTextField.getText());
        personCreateView.setPwd(newPwd.getText().toCharArray());
        personCreateView.setResultID(Integer.valueOf(Result.getText()));
        personCreateView.setID(Integer.valueOf(newID.getText()));

        personService.createPerson(personCreateView);

        personCreatedConfirmationDialog();
    }


    @FXML
    public void createChoice(ActionEvent actionEvent) {
        String newResult = newresultChoiceBox.getValue();
        switch (newResult) {
            case "Vaccinated":
                Result.setText("1");
                break;

            case "Recovered":
                Result.setText("2");
                break;

            case "Tested positive":
                Result.setText("3");
                break;

            case "Tested negative":
                Result.setText("4");
                break;

            case "Hospitalized":
                Result.setText("5");
                break;

            case "Passed away":
                Result.setText("6");
                break;

            default:
                break;
        }
    }


    private void personCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Person Created Confirmation");
        alert.setHeaderText("New person was successfully added.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
