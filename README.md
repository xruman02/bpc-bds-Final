# BDS - JavaFX Demo Template
This project is a Java Desktop GUI used for handling the database made in the first project.

## To build&run the project
Enter the following command in the project root directory to build the project.
```shell
$ mvn clean install
```

Run the project:
```shell
$ java -jar target/bds-javafx-training-1.0.0.jar
```


To log in, use following credentials: (a random person from the database)
For example:
- Username: `Landon.Hanson@email.com`
- Password: `password`

## To generate the project and external libraries licenses
Enter the following command in the project root directory
```shell
$ mvn project-info-reports:dependencies
```


